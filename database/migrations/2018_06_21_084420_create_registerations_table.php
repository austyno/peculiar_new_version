<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegisterationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registerations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('formnum')->unsigned();
            $table->string('address');
            $table->integer('age')->unsigned();
            $table->string('project');
            $table->integer('amount')->unsigned();
            $table->string('state');
            $table->string('lga');
            $table->string('next_of_kin');
            $table->string('next_of_kin_tel');
            $table->string('referee_name');
            $table->string('referee_tel');
            $table->string('referee_address');
            $table->string('passport');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registerations');
    }
}
