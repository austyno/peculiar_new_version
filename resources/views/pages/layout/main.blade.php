<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title></title>

<!-- Stylesheets -->
<link href="{{ asset('css/bootstrap.css')}}" rel="stylesheet">
<link href="{{ asset('css/responsive.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/animate.min.css')}}"/>
<link href="{{ asset('css/style.css')}}" rel="stylesheet">

<!-- mobile responsive meta -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!--Favicon-->
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="icon" href="favicon.ico" type="image/x-icon">

</head>

<body>
<div class="page-wrapper"> 	

    <!-- Preloader -->
    <div class="preloader"></div>
    <!-- Preloader -->
    

	<!--header top-->
    <div class="header-top">
        <div class="container clearfix">
            <div class="top-left">
                <ul class="contact-links">
                    <li><i class="fa fa-clock-o" aria-hidden="true"></i>Mon - Sat  9.00 AM - 6.00 PM</li>
                    <li><i class="fa fa-phone" aria-hidden="true"></i>+234706677294, +2348079579211</li>
                </ul>
            </div>
            <div class="top-right clearfix">
                <ul class="social-links">
                    <li><a href="https://www.facebook.com/Peculiar-Family-Co-operative-Society-Limited-222751434983372/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="https://twitter.com/peculiarcop"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                </ul>
                <div class="search_option">
                    <a href="#"><span class="icon fa fa-search"></span></a>
                </div>
            </div>
        </div>
    </div>
    <!--header top-->


    <!--Header Upper-->
    <section class="header-upper">
        <div class="container clearfix">
            <div class="logo">
                <figure>
					<h1 style="font-size:20px;float:right;line-height:25px;margin-top:7px;padding:7px">Peculiar Gracious<br> Family Foundation</h1>
					<a href="{{ route('home')}}"><img src="images/logo.jpeg" alt="" height="70"></a>
				</figure>
				
            </div>
            <ul class="header-info">
                <li class="item">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <h6>Email Us:</h6>
                    <a href="#"><span>info@peculiarfamilycop.org</span></a>
                </li>
                <li class="item">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    <h6>location</h6>
                    <span>Suite 004 Apple Plaza, Area A last Road <span>Nyanya Abuja</span>
                </li>
                {{-- <li class="item">
                    <i class="fa fa-user-o" aria-hidden="true"></i>
                    <h6>login</h6>
                    <span>charity founder</span>
                </li> --}}
            </ul>
        </div>
    </section>
    <!--End Header Upper-->


    <!--Main Header-->
    <header class="main-header sticky-header">
        <div class="container">
            <div class="header-area clearfix">                
                <nav class="main-menu">
                    <div class="navbar-header">
                        <!-- Toggle Button -->      
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>           
                    <div class="navbar-collapse collapse clearfix">
                        <ul class="navigation clearfix">
                            <li class="current"><a href="index.html">Home</a></li>
                        <li><a href="#">About</a></li>
                            <li><a href="#">Programmes</a></li>
                            <li class="dropdown"><a href="#">Events&nbsp;&nbsp; <span class="fa fa-angle-down"></span></a>
                                <ul>
                                    <li><a href="events.html">Events</a></li>
                                    <li><a href="events-list.html">Gallery</a></li>
                                </ul>                                
                            </li>                            
                            <li><a href="contact.html">Contact us</a></li>
                        </ul>
                    </div>                    
                </nav>
                <div class="link-button">
                    <a href="#" class="btn-style-one">Donate now</a>
                </div>
            </div>
        </div>
    </header>
	<!--End Main Header -->
	@yield('content')

	<footer class="main-footer" style="background: url(images/background/3.jpg);">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="about-widget">
                            <div class="footer-logo">
                                <figure>
										<h1 style="font-size:20px;float:right;line-height:25px;margin-top:7px;padding:7px">Peculiar Gracious<br> Family Foundation</h1>
                                    <a href="index.html"><img src="images/logo.jpeg" alt="" height="70" style="border-radius:5px"></a>
                                </figure>
                            </div>
                            <p>Connect with us on Social Media</p>
                            <ul class="social-links">
                                <li><a href="https://www.facebook.com/Peculiar-Family-Co-operative-Society-Limited-222751434983372/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="https://twitter.com/peculiarcop"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									
                            </ul>
						</div> 
					</div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="gallery-widget">
                            <div class="sec-title">
                                <h5>Our Photos</h5>
                            </div>                              
                            <div class="clearfix">
                                <figure class="image">
                                    <img src="images/gallery/1.jpg" alt="" height="78">
                                    <a href="images/gallery/1.jpg" class="lightbox-image" title=""><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </figure>
                                <figure class="image">
                                    <img src="images/gallery/2.jpg" alt="" height="78">
                                    <a href="images/gallery/2.jpg" class="lightbox-image" title=""><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </figure>
                                <figure class="image">
                                    <img src="images/gallery/3.jpg" alt="" height="78">
                                    <a href="images/gallery/3.jpg" class="lightbox-image" title=""><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </figure>
                                <figure class="image">
                                    <img src="images/gallery/4.jpg" alt="" height="78">
                                    <a href="images/gallery/4.jpg" class="lightbox-image" title=""><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </figure>
                                <figure class="image">
                                    <img src="images/gallery/5.jpeg" alt="" height="78">
                                    <a href="images/gallery/5.jpeg" class="lightbox-image" title=""><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </figure>
                                <figure class="image">
                                    <img src="images/gallery/6.jpg" alt="" height="78">
                                    <a href="images/gallery/6.jpg" class="lightbox-image" title=""><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </figure>
                            </div>
						</div> 
						
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="news-widget">
                            <div class="sec-title">
                                <h5>Events</h5>
                            </div>                            
                            <ul class="news-links">
                                <li class="item">
                                    <i>30 jul</i>
                                    <a href="single-blog.html"><h6>Child Care Centers</h6></a>
                                    <a href="#"><p>view</p></a>
                                </li>
                                <li class="item">
                                    <i>26 may</i>
                                    <a href="single-blog.html"><h6>Disaster Relief</h6></a>
                                    <a href="#"><p>view</p></a>
                                </li>
                                <li class="item">
                                    <i>23 feb</i>
                                    <a href="single-blog.html"><h6>Arts & Culture</h6></a>
                                    <a href="#"><p>view</p></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="contact-widget">
                            <div class="sec-title">
                                <h5>contact us</h5>
                            </div>
                            <ul class="contact-list">
                                <li class="item">
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                    <p>0706677294 <br> 08079579211 </p>
                                </li>
                                <li class="item">
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    <a href="#"><p>info@peculiarfamilycop.org <br>info@peculiarfamilycop.org</p></a>
                                </li>
                                <li class="item">
                                    <i class="fa fa-home" aria-hidden="true"></i>
                                    <p>Suite 004 Apple Plaza, Area A last Road Nyanya Abuja.</p>
                                </li>
                            </ul>
                        </div>                                 
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="copyright-text">
                    <p class="pull-right"> Powered by <a href="#"><i>SurgeStone Technologies</i></a></p>
                    <p style="width:40%"><a href="{{ route('home')}}">PGFF</a> &copy; 2019 All Right Reserved </p>
                    
                </div>
            </div>
        </div>    
    </footer>
    <!--End main-footer-->


</div>
<!--End pagewrapper-->



<!--Search Popup-->
<div id="search-popup" class="search-popup">
    <div class="close-search theme-btn"><span class="flaticon-cancel"></span></div>
    <div class="popup-inner">
        <div class="search-form">
            <form method="post" action="index.html">
                <div class="form-group">
                    <fieldset>
                        <input type="search" class="form-control" name="search-input" value="" placeholder="Search Here" required >
                        <input type="submit" value="Search" class="theme-btn">
                    </fieldset>
                </div>
            </form>
            <br>
            <h3>Recent Search Keywords</h3>
            <ul class="recent-searches">
                <li><a href="#">Business</a></li>
                <li><a href="#">Web Development</a></li>
                <li><a href="#">SEO</a></li>
                <li><a href="#">Logistics</a></li>
                <li><a href="#">Freedom</a></li>
            </ul>
        </div>
    </div>
</div>

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".header-top"><span class="icon fa fa-angle-up"></span></div>

<script src="{{ asset('js/jquery.js')}}"></script>
<script src="{{ asset('js/bootstrap.min.js')}}"></script>
<script src="{{ asset('js/isotope.js')}}"></script>
<script src="{{ asset('js/jquery.fancybox.pack.js')}}"></script>
<script src="{{ asset('js/jquery.fancybox-media.js')}}"></script>
<script src="{{ asset('js/html5lightbox.js')}}"></script>
<!-- circle progress -->
<script src="{{ asset('js/circle-progress.js')}}"></script>

<!-- revolution slider js -->
<script src="{{ asset('assets/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{ asset('assets/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
<script src="{{ asset('assets/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script src="{{ asset('assets/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
<script src="{{ asset('assets/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script src="{{ asset('assets/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script src="{{ asset('assets/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script src="{{ asset('assets/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script src="{{ asset('assets/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
<script src="{{ asset('assets/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script src="{{ asset('assets/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>

<script src="{{ asset('js/owl.carousel.min.js')}}"></script>
<script src="{{ asset('js/validate.js')}}"></script>
<script src="{{ asset('js/wow.js')}}"></script>
<script src="{{ asset('js/jquery.appear.js')}}"></script>
<script src="{{ asset('js/jquery.countTo.js')}}"></script>

<script src="{{ asset('js/script.js')}}"></script>
</body>
</html>