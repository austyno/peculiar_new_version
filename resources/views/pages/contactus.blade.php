@extends('pages.layout.main')

@section('content')

<div class="banner">
        <h2>Contact Us</h2>
        <p><a href="{{ route('home') }}">Home »</a> Contact us </p>
    </div>

    <section class="contact" id="contact">
        <div class="container">
            <div class="contact-heading">
                <h3 class="heading"></h3>
            </div>
            <div class="contact-grids">
                <div class=" col-md-5 contact-form" style="background:linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5));">
                <h4 class="heading">Get In Touch</h4>
                    <form action="#" method="post">
                            <input type="text" placeholder="Full Name" required=""/>
                            <input type="email" placeholder="Your Mail" required=""/>
                            <input type="text" placeholder="Mobile Number" required=""/>
                            <textarea placeholder="Message" required=""></textarea>
                            <div class="submit1">
                                <input type="submit" value="sent" style="color:#fefde9;background:#0f1f52;border:.3px solid #fefde9">
                            </div>
                    </form>
                </div>
                <div class=" col-md-7 map">
                    <iframe 
  width="300" 
  height="170" 
  frameborder="0" 
  scrolling="no" 
  marginheight="0" 
  marginwidth="0" 
  src="https://maps.google.com/maps?q='+39.041207+','+-94.590044+'&hl=es;z=14&amp;output=embed"
 >
 </iframe>
                    <div class="col-md-4 contact-grid1">
                        <i class="fa fa-map-marker" aria-hidden="true" style="color:#0f1f52"></i>
                        <div class="contact-right" style="color:#fefde9">
                            <p style="color:#fefde9">Address</p>
                            Suite 004 Apple Plaza, Area A last Road Nyanya Abuja
                        </div>
                    </div>
                    <div class="col-md-4 contact-grid1">
                        <i class="fa fa-phone" aria-hidden="true" style="color:#0f1f52"></i>
                        <div class="contact-right">
                            <p style="color:#fefde9">Phone</p>
                            <span style="color:#fefde9">07066077294</span>
                            <span style="color:#fefde9">08079579211</span>
                        </div>
                    </div>
                    <div class="col-md-4 contact-grid1">
                        <i class="fa fa-envelope" aria-hidden="true" style="color:#0f1f52"></i>
                        <div class="contact-right">
                            <p style="color:#fefde9">Email</p>
                            <a href="mailto:info@peculiarfamilycop.org" style="color:#fefde9">info@peculiarfamilycop.org</a>
                            <a href="mailto:info@peculiarfamilycop.org" style="color:#fefde9">info@peculiarfamilycop.org</a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- //contact -->
    

@endsection