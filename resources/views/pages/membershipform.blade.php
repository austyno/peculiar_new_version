@extends('pages.layout.main')

@section('content')


<div class="banner">
        <h2>Membership Form</h2>
        <p><a href="{{ route('home') }}">Home »</a> Membership Form </p>
    </div>

    <div class="faq-content" style="margin-top:0px">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <p style="color:#fff;font-size:25px">To access the Membership form, Please pay 1500 naira by clicking the pay button below. </p>
                <form method="POST" action="{{ route('pay') }}" accept-charset="UTF-8" class="form-horizontal" role="form">
                    <div class="row" style="margin-bottom:40px;">
                      <div class="col-md-8 col-md-offset-2">
                        <p>
                            <div style="color:white">
                                Peculiar Family cooperative membership form  
                                ₦1500.00
                            </div>
                        </p>
                        
                        <input type="hidden" name="email" value="peculiarfamilycop@gmail.com"> {{-- required --}}
                        <input type="hidden" name="amount" value="150000"> {{-- required in kobo --}}
                        <input type="hidden" name="quantity" value="1">
                        <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> {{-- required --}}
                        <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}"> {{-- required --}}
                        {{ csrf_field() }} {{-- works only when using laravel 5.1, 5.2 --}}
            
                         {{-- employ this in place of csrf_field only in laravel 5.0 --}}
                        <input type="hidden" name="callback_url" value="https://peculiarfamilycop.org/paystackcallback">
            
                        <p>
                          <button class="btn btn-default btn-lg btn-block" type="submit" value="Pay Now!" style="background:#0f1f52;color:white">
                          <i class="fa fa-plus-circle fa-lg"></i> Pay Now!
                          </button>
                        </p>
                      </div>
                    </div>
            </form>
            </div>
        </div>

</div>







@endsection