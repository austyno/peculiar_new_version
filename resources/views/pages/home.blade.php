@extends('pages.layout.main')
<style>
    /* .about-section .item-holder:hover{
        box-shadow: 0px 5px 20px 0px rgba(255, 255, 255, 0.38);
    } */
</style>
@section('content')
<section class="">
        <div id="myCarousel" class="carousel slide carousel-fade">
            <!-- Carousel indicators -->
            <!-- <ol class="carousel-indicators">
               <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
               <li data-target="#myCarousel" data-slide-to="1"></li>
               <li data-target="#myCarousel" data-slide-to="2"></li>
         </ol> -->
            <!-- Carousel items -->
            <div class="carousel-inner">
               <div class="item active" style="height:500px;">
                  <img  src="images/slider/tailor.jpg" alt="First slide" width="" height=""  style="background-size:cover">
                    <div class="slide-content-box text-center text-box animated zoomIn">
                            <h3 style="color:white">To train, empower and educate the youths</h3>
                            <h1 style="color:white;font-size:60px;">We need your support</h1>
                    </div>
                    <div class="button text-center btn-box animated slideInUp" style="right:50%">
                        <a href="#" class="btn-style-one">contact us</a>     
                    </div>
                    <div class="button pull-right btn-box animated slideInUp" style="left:55%">
                        <a href="#" class="btn-style-two">read more</a>     
                    </div>
               </div>
               <div class="item" style="height:500px;">
                    <img src="images/slider/market.jpg" alt="Second slide">
                    <div class="slide-content-box text-center text-box animated zoomIn">
                        <h3 class="animated fadeInRight" style="color:white">We provide small soft loans to allieviate poverty.</h3>
                        <h1 style="color:white;font-size:60px;">We need your support</h1>
                    </div>
                    <div class="button text-center btn-box animated slideInUp" style="right:50%">
                            <a href="#" class="btn-style-one">contact us</a>     
                    </div>
                    <div class="button pull-right btn-box animated slideInUp" style="left:55%">
                            <a href="#" class="btn-style-two">read more</a>     
                    </div>
               </div>
         </div>
            <!-- Carousel nav -->
            <a class="carousel-control left " href="#myCarousel"
               data-slide="prev">&lsaquo;</a>
            <a class="carousel-control right" href="#myCarousel"
               data-slide="next">&rsaquo;</a>
         </div>
    </section>
    
    {{-- About --}}
    <section class="about-section" style="">
            <div class="container">
                <div class="section-title">
                    <h2 class="">about us</h2>
                    <p>Peculiar Gracious Family Foundation has the sole vision and purpose of improving the standard of living of its members and potential members</p>
                </div>
                <div class="three-column-carousel">
                    <div class="item-holder">
                        <div class="image-box">
                            <figure>
                                <a href="#"><img class="abtImg" src="images/resource/market.jpg" alt=""></a>
                            </figure>
                        </div>
                        <div class="image-content">
                            <a href="#"><h6>soft Loans and Grants </h6></a>
                            <p>In line with the vision of the Foundation, we provide soft loans and grants to individuals to enable them upgrade their businesses and consequently improve their lives. </p>
                            <div class="link-btn">
                                <a href="#" class="btn-style-one">read more</a>
                            </div>
                        </div>
                    </div>
                    <div class="item-holder">
                        <div class="image-box">
                            <figure>
                                <a href="#"><img class="abtImg" src="images/resource/serv.jpg" alt=""></a>
                            </figure>
                        </div>
                        <div class="image-content">
                            <a href="#"><h6>Wealth Management.</h6></a>
                            <p>we help our registered members with professional services that combines both financial and investment advice. "Wealth that is not properly managed will soon be lost."</p>
                            <div class="link-btn">
                                <a href="#" class="btn-style-one">read more</a>
                            </div>
                        </div>
                    </div>
                    <div class="item-holder">
                        <div class="image-box">
                            <figure>
                                <a href="#"><img class="abtImg" src="images/resource/wealth.jpg" alt=""></a>
                            </figure>
                        </div>
                        <div class="image-content">
                            <a href="#"><h6>Wealth Creation</h6></a>
                            <p>We help members to create fresh ideas that can be converted into wealth, explore new financial breakthroughs, overcome their fears and expose them to areas of wisdom and adventures for future life actualization. </p>
                            <div class="link-btn">
                                <a href="#" class="btn-style-one">read more</a>
                            </div>
                        </div>
                    </div>
                    <div class="item-holder">
                        <div class="image-box">
                            <figure>
                                <a href="#"><img class="abtImg" src="images/resource/seminar.jpg" alt=""></a>
                            </figure>
                        </div>
                        <div class="image-content">
                            <a href="#"><h6>Empowerment</h6></a>
                            <p>We conduct seminars and entrepreneurial trainings for the members of our Foundation to provide them with the necessary information and direction they need to connect with their goals and make intelligent financial decisions. </p>
                            <div class="link-btn">
                                {{-- link to programes --}}
                                <a href="#" class="btn-style-one">read more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="wellcome-section" style="background: url(images/background/6.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="image-box">
                            <figure>
                                <img src="images/resource/5.jpg" alt="">
                            </figure>
                            <a href="https://youtu.be/juEo4ows7Po" class="html5lightbox" title=""><i class="flaticon-play-button"></i></a>
                        </div>                    
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="image-text">
                            <h6>sponsor this entire project</h6>
                            <h3>Empower 100 youths</h3>
                            <p>We believe that it is the people that make up the society and the current situation of the people can either make or mar the society.</p>
                            <div class="link-buttons">
                                <a href="#" class="btn-style-two">read more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="our-volunteer">
                <div class="container">
                    <div class="section-title text-center">
                        <h2>our Team</h2>
                        <p>Our strength is in the dedicated and tireless men and women who are passionate about changing lives.</p>
                    </div>
                    <div class="row" style="">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="item-holder">
                                <div class="image-box">
                                    <figure>
                                        <img src="images/resource/peculiar1.jpeg" alt="" style="height:350px;width:auto">
                                    </figure>
                                    <ul class="social-links">
                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                                <div class="image-title text-center">
                                    <h5>Pastor Ezekiel Bitrus <br>Nyakpan</h5>
                                    <p>Chairman/National Coordinator<br> and Founder</p>
                                </div>                        
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="item-holder">
                                <div class="image-box">
                                    <figure>
                                        <img src="images/resource/peculiar2.jpeg" alt="" style="height:350px" width="auto">
                                    </figure>
                                    <ul class="social-links">
                                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                        </ul>
                                </div>
                                <div class="image-title text-center">
                                    <h5>Mrs. Grace Usara John</h5>
                                    <p>National Treasurer</p>
                                </div>                        
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="item-holder">
                                <div class="image-box">
                                    <figure>
                                        <img src="images/resource/peculiar3.jpeg" alt="" style="height:350px;" width="auto">
                                    </figure>
                                    <ul class="social-links">
                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                                <div class="image-title text-center">
                                    <h5>Mr. Ezekiel Love Taenyi</h5>
                                    <p>National Secretary</p>
                                </div>                        
                            </div>
                        </div>
                    </div>
                </div>
            </section>
    
            <section class="features-section" style="background: url(images/background/5.jpg);background-size:cover">
                <div class="container">
                    <div class="content-text text-center">
                        <h5>Charity For Education</h5>
                        <h2>Partner With US: Volunteering or Donating</h2>
                        <p>Join forces with us as we strive to make our society better one individual at a time.</p>
                        <div class="link-btn">
                            <a href="#" class="btn-style-one">Join Us</a>
                            <a href="#" class="btn-style-two">Donate Now</a>
                        </div>
                    </div>
                </div>
            </section>

            <section class="our-couses">
                    <div class="container">
                        <div class="section-title text-center">
                            <h2>our Projects</h2>
                            <p>Our projects are aimed at improving the standard of living of every Nigerian and the Society at large.</p>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="item-holder">
                                    <div class="image-box">
                                        <figure>
                                            <img src="images/resource/agro.jpg" alt="">
                                        </figure>
                                        <div class="causes-progress">
                                            <div class="progress-item">
                                                <div class="progress" data-value="40">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                                                        <div class="value-holder"><span class="value"></span>%</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content-text">
                                        {{-- <ul>
                                            <li>Raised: N 400,000</li>
                                            <li>Goal: N 1,000,000</li>
                                        </ul> --}}
                                        <h3>Women In Agric</h3>
                                        <p>This is aimed at providing soft loans to rural women for subsitence agric and trade through organized co operatives </p>
                                    </div>
                                    <div class="link-btn">
                                        <a href="#" class="btn-style-one">donate now</a>
                                    </div>                         
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="item-holder">
                                    <div class="image-box">
                                        <figure>
                                            <img src="images/resource/tailor.jpg" alt="" style="height:270px">
                                        </figure>
                                        <div class="causes-progress">
                                            <div class="progress-item">
                                                <div class="progress" data-value="70">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">
                                                        <div class="value-holder"><span class="value"></span>%</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content-text">
                                        {{-- <ul>
                                            <li>Raised: $30,000</li>
                                            <li>Goal: $70,000</li>
                                        </ul> --}}
                                        <h3>Skills Aqusition</h3>
                                        <p>Every 90 days we graduate youths trained in Hair dressing, Fashion design, Phone repairs etc, from our skill aqusition centers.</p>
                                    </div>
                                    <div class="link-btn">
                                        <a href="#" class="btn-style-one">donate now</a>
                                    </div>                          
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="item-holder">
                                    <div class="image-box">
                                        <figure>
                                            <img src="images/resource/4.jpg" alt="" height="270px">
                                        </figure>
                                        <div class="causes-progress">
                                            <div class="progress-item">
                                                <div class="progress" data-value="70">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">
                                                        <div class="value-holder"><span class="value"></span>%</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content-text">
                                        {{-- <ul>
                                            <li>Raised: $55,000</li>
                                            <li>Goal: $90,000</li>
                                        </ul> --}}
                                        <h3>Off-The-Streets</h3>
                                        <p>We organize sensitization seminars once every month to create awareness about the dangers of cultism, drug abuse and prostitution.</p>
                                    </div>
                                    <div class="link-btn">
                                        <a href="#" class="btn-style-one">donate now</a>
                                    </div>                           
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="fact-counter" style="background: url(images/background/3.jpg);">
                    <div class="container">
                        <div class="row clearfix">
                            <div class="counter-outer clearfix">
                                <!--Column-->
                                <article class="column counter-column col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-duration="0ms">
                                    <div class="item">
                                        <div class="count-outer">
                                            <div class="icon">
                                                <i class="flaticon-layers"></i>
                                            </div>                                
                                            <span class="count-text" data-speed="3000" data-stop="15">0</span>
                                        </div>
                                        <h4 class="counter-title">Disaster Relief Programs</h4>
                                    </div>                            
                                </article>
                                
                                <!--Column-->
                                <article class="column counter-column col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-duration="0ms">
                                    <div class="item">
                                        <div class="count-outer">
                                            <div class="icon">
                                                <i class="flaticon-social"></i>
                                            </div>
                                            <span class="count-text" data-speed="3000" data-stop="150">0</span>
                                        </div>
                                        <h4 class="counter-title">Youths Trained</h4>
                                    </div>
                                </article>
                                
                                <!--Column-->
                                <article class="column counter-column col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-duration="0ms">
                                    <div class="item">
                                        <div class="count-outer">
                                            <div class="icon">
                                                <i class="flaticon-cup"></i>
                                            </div>
                                            <span class="count-text" data-speed="3000" data-stop="70">0</span>
                                        </div>
                                        <h4 class="counter-title">Charitable Endowments</h4>
                                    </div>
                                </article>
                                
                                <!--Column-->
                                <article class="column counter-column col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-duration="0ms">
                                    <div class="item">
                                        <div class="count-outer">
                                            <div class="icon">
                                                <i class="flaticon-trophy"></i>
                                            </div>
                                            <span class="count-text" data-speed="3000" data-stop="100">0</span>
                                        </div>
                                        <h4 class="counter-title">Women Empowered</h4>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="gallery-section">
                    <div class="container">
                        <div class="section-title text-center">
                            <h2>our gallery</h2>
                            <p></p>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="image-box">
                                    <figure>
                                        <img src="images/gallery/10.jpg" alt="">
                                    </figure>
                                    <a href="images/gallery/10.jpg" class="lightbox-image" title=""><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="image-box">
                                    <figure>
                                        <img src="images/gallery/11.jpg" alt="">
                                    </figure>
                                    <a href="images/gallery/11.jpg" class="lightbox-image" title=""><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="image-box">
                                    <figure>
                                        <img src="images/gallery/12.jpg" alt="">
                                    </figure>
                                    <a href="images/gallery/12.jpg" class="lightbox-image" title=""><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="image-box">
                                    <figure>
                                        <img src="images/gallery/13.jpg" alt="">
                                    </figure>
                                    <a href="images/gallery/13.jpg" class="lightbox-image" title=""><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="image-box">
                                    <figure>
                                        <img src="images/gallery/14.jpg" alt="">
                                    </figure>
                                    <a href="images/gallery/14.jpg" class="lightbox-image" title=""><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="image-box">
                                    <figure>
                                        <img src="images/gallery/15.jpg" alt="">
                                    </figure>
                                    <a href="images/gallery/15.jpg" class="lightbox-image" title=""><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="sponsor-logo">
                    <div class="container" style="">
                        <div class="text-center quote" style="font-size:25px;">
                            <p class="lead">
                                <em>“Great works are performed not by strength, but perseverance“</em>                
                            </p>
                        </div>
                        <span class="text-center" style="margin-left:40%">Dr. Samuel Johnson</span>
                    </div>
                </section>
@endsection