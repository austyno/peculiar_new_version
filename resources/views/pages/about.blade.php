@extends('pages.layout.main')

@section('content')
<!--//banner -->
<div class="banner" style="">
	<h2 style="color:#fefde9">About Us</h2>
	<p><a href="{{ route('home') }}">Home »</a> About us </p>	
</div>

<!-- banner bottom -->
<div class="welcome-about">
		<div class="layer" style="">
				<h3 class="heading"></h3>
				<div class="container">
					<div class="who_grids">
						<div class="col-md-6 who_grid_left" style="">
							<h3 style="color:#fefde9">Who we are</h3>
						
							<p>The Peculiar Family Co-operative society Limited is a vision concieved and birthed from the heart of a philanthropist and a man with great drive to see that the economic wellbeing of individuals and families is improved on daily bases</p>
							<div style="width:200px;float:left">
								<p class="listing"><i class="fa fa-chevron-right" aria-hidden="true"></i>credit.</p>
								<p class="listing"><i class="fa fa-chevron-right" aria-hidden="true"></i> loans.</p>
								<p class="listing"><i class="fa fa-chevron-right" aria-hidden="true"></i> grant.</p>
							</div>
							<div style="float:leftt;margin-top:0px">
								<p class="listing"><i class="fa fa-chevron-right" aria-hidden="true"></i> Savings</p>
								<p class="listing"><i class="fa fa-chevron-right" aria-hidden="true"></i> Agricultural Implements.</p>
								<p class="listing"><i class="fa fa-chevron-right" aria-hidden="true"></i> Agro chemicals and fertilizers.</p>
							</div>
							<div class="read">
									<a href="#" data-toggle="modal" data-target="#myModal">Read more »</a>
								</div>
							
						</div>
						<div class="col-md-6 who_grid_right">
							<h2 style="color:#fefde9">Objectives</h2>
							<p>The objective of the society shall be the promotion of the economic interest of its members and most especially to engage in the production, purchase and resell of essential and scares commodities to members at a reasonable price.</p>
							
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
	
</div>
<!-- //banner bottom -->

<!-- Who we are -->

<!-- //Who we are -->

<!-- pricing plans -->
<div class="pricing">
<h3 class="heading" style="color:white">The Team</h3>
	<div class="container">
		<div class="w3l-pricing-grids">
			<div class="agileits-pricing-grid first" >
				<div class="pricing_grid">
					<div class="pricing-top" style="background:#cccccc">
						<h3 style="font-size:20px">Pastor Ezekiel Bitrus Nyakpan</h3>
					</div>
						
					<div class="pricing-bottom">
							<img src="{{asset('images/peculiar1.jpeg')}}" height="320px" width="356px">
						
						<div class="buy-button">
							<p>A man of great integrity, influence and Humility. Chairman/National Coordinator and Founder of Peculiar Family Cooperative society Limited</p>
						</div>
					</div>
				</div>
			</div>
			<div class="agileits-pricing-grid second">
				<div class="pricing_grid">
					<div class="pricing-top" style="background:#cccccc">
						<h3 style="font-size:20px">Mrs. Grace Usara John</h3><br>
					</div>
					<div class="pricing-bottom">
						<img src="{{ asset('images/peculiar2.jpeg') }}" height="320px" width="350px">
						<div class="buy-button">
							<p>She's a principled, ethical and value oriented individual. She's the Treasurer Peculiar Family Cooperative Society Limited</p>
						</div>
					</div>
				</div>
			</div>
			<div class="agileits-pricing-grid third">
					<div class="pricing-top" style="background:#cccccc">
						<h3 style="font-size:20px">Mr. Ezekiel Love Taenyi</h3><br>
					</div>
						
					<div class="pricing-bottom">
						<img src="{{ asset('images/peculiar3.jpeg') }}" height="320px" width="350px">
						<div class="buy-button">
							<p>A philanthropist, social, ethical and nature lover. He's the secretary Peculiar Family Cooperative Society Limited</p>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>

<div id="popup">
		<div id="small-dialog" class="mfp-hide">
			<div class="signin-form profile">
				<h3>Sign Up</h3>
				<div class="login-form">
					<form action="#" method="post">
						<input type="email" name="email" placeholder="E-mail" required="">
						<input type="text" name="name" placeholder="Username" required="">
						<input type="password" name="password" placeholder="Password" required="">
						<input type="password" name="password" placeholder="Confirm Password" required="">
						<input type="submit" value="Sign Up">
					</form>
				</div>
			</div>
		</div>
	</div>
<!-- //pricing plans -->


@endsection