<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registeration extends Model
{
    protected $fillable = [
        'name','address','age','project','amount','state','lga','next_of_kin','next_of_kin_tel','referee_name','referee_tel','referee_address','passport','formnum'

    ];
}
